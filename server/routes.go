package server

import (
	"net/http"
	"sgl/server/handlers/user"
)

// An API route
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
	Queries     map[string]string
}

// Slice of all API routes
type Routes []Route

var routes = Routes{
	Route{
		Name:        "LoginUser",
		Method:      "POST",
		Pattern:     "/login",
		HandlerFunc: user.LoginUser,
	},
	/*	Route{
			Name:        "Index",
			Method:      "GET",
			Pattern:     "/",
			HandlerFunc: handlers.Index,
		},
		Route{
			Name:        "CreateUser",
			Method:      "POST",
			Pattern:     "/api/users",
			HandlerFunc: user.CreateUser,
		},

		Route{
			Name:        "GetOwner",
			Method:      "GET",
			Pattern:     "/api/users/{id}/owners",
			HandlerFunc: user.GetOwner,
		},
		Route{
			Name:        "GetStables",
			Method:      "GET",
			Pattern:     "/api/owners/{id}/stables",
			HandlerFunc: owner.GetStables,
		},
		Route{
			Name:        "GetJarsByOwnerId",
			Method:      "GET",
			Pattern:     "/api/owners/{id}/jars",
			HandlerFunc: owner.GetJars,
		},
		Route{
			Name:        "GetJarsByStableId",
			Method:      "GET",
			Pattern:     "/api/stables/{id}/jars",
			HandlerFunc: stable.GetJars,
		},
		Route{
			Name:        "GetSnailsByOwnerId",
			Method:      "GET",
			Pattern:     "/api/owners/{id}/snails",
			HandlerFunc: owner.GetSnails,
		},
		Route{
			Name:        "GetSnailsByJarId",
			Method:      "GET",
			Pattern:     "/api/jars/{id}/snails",
			HandlerFunc: jar.GetSnails,
		},
		Route{
			Name:        "TestAuth",
			Method:      "GET",
			Pattern:     "/api/session/status",
			HandlerFunc: user.TestAuth,
		},
		Route{
			Name:        "CreateOwner",
			Method:      "POST",
			Pattern:     "/api/owners",
			HandlerFunc: owner.CreateOwner,
		},
		Route{
			Name:        "CreateStable",
			Method:      "POST",
			Pattern:     "/api/stables",
			HandlerFunc: stable.CreateStable,
		},
		Route{
			Name:        "CreateJar",
			Method:      "POST",
			Pattern:     "/api/jars",
			HandlerFunc: jar.CreateJar,
		},
		Route{
			Name:        "CreateSnail",
			Method:      "POST",
			Pattern:     "/api/snails",
			HandlerFunc: snail.CreateSnail,
		},
		Route{
			Name:        "UpdateOwner",
			Method:      "POST",
			Pattern:     "/api/owners/{id}",
			HandlerFunc: owner.Update,
		},
		Route{
			Name:        "UpdateSnail",
			Method:      "POST",
			Pattern:     "/api/snails/{id}",
			HandlerFunc: snail.Update,
		}, */
}
