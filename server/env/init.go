package env

import (
	"fmt"
	"github.com/spf13/viper"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"io/ioutil"
	"os"
	"runtime"
	"strings"
)

type AppConfig struct {
	SnailLifeServerUrl string
	AuthProvider       string
}

var App AppConfig

// configure reads the current environment from env.conf
// and loads the associated Configuration files.
func Congfigure(confdir string) (err error) {
	envname := os.Getenv("envname")
	if len(envname) == 0 {
		en, err := ioutil.ReadFile(confdir + "/env.conf")
		if err != nil {
			error2.HandleErr(err, "")
			return err
		}
		envname = string(en)
	}
	fmt.Printf("\nEnv name: %s\n", envname)
	configPath := confdir + "/" + envname
	viper.AddConfigPath(configPath)
	cwd, _ := os.Getwd()
	viper.AddConfigPath(cwd)
	// db
	viper.SetConfigName("config")
	err = viper.ReadInConfig()
	if err != nil {
		error2.HandleErr(err, "")
		return err
	}
	appc := AppConfig{}
	err = viper.Unmarshal(&appc)
	if err != nil {
		error2.HandleErr(err, "")
		return err
	}
	App = appc
	return err
}

func GetProjectRootPath() string {
	_, b, _, _ := runtime.Caller(0)
	folders := strings.Split(b, "/")
	folders = folders[:len(folders)-3]
	path := strings.Join(folders, "/")
	return path
}
