package handlers

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/clientlib/user"
	"gitlab.com/drakonka/gosnaillife/common"
	user2 "gitlab.com/drakonka/gosnaillife/common/domain/user"
	"html/template"
	"net/http"
	"os"
	"sgl/server/env"
)

func Home(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Home")
	tmpl, err := template.ParseFiles(env.GetProjectRootPath() + "templates/index.html")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, err = w.Write([]byte(fmt.Sprintf("Could not parse template file: %v", err)))
		if err != nil {
			common.Log.Error(err)
		}
		return
	}
	// Get user from cookie
	c, err := r.Cookie("username")
	if err != nil {
		msg := fmt.Sprintf("Couldn't get username cookie: %v", err)
		w.Write([]byte(msg))
		return
	}
	username := c.Value

	// Get token from cookie
	c, err = r.Cookie("token")
	if err != nil {
		msg := fmt.Sprintf("Couldn't get auth token: %v", err)
		w.Write([]byte(msg))
		return
	}
	token := c.Value
	ap := r.Header.Get(env.App.AuthProvider)
	var res *http.Response
	res, err = user.Authenticate(env.App.SnailLifeServerUrl, ap, map[string]string{"authorization": token})

	var data map[string]interface{}
	if err != nil {
		data["err"] = fmt.Sprintf("Unable to authenticate due to error: %v", err)

	} else if res.StatusCode != http.StatusOK {
		data["err"] = fmt.Sprintf("Unable to authenticate...")
	} else {
		data["user"] = user2.User{Email: username}
	}

	tmpl.Execute(os.Stdout, data)

}
