module sgl

go 1.12

require (
	github.com/google/uuid v1.1.1 // indirect
	github.com/gorilla/mux v1.7.2
	github.com/gorilla/sessions v1.2.0
	github.com/pkg/errors v0.8.1 // indirect
	github.com/spf13/viper v1.2.1
	gitlab.com/drakonka/gosnaillife v0.0.0-20190809182823-3a81e88d496e
)
